using System.Collections.Generic;
using System.Linq;

namespace TestingSession.Sorting
{
    public class HardCodedCoarchSorter : ICoachSorter
    {
        private CoachSetter _setter = new CoachSetter(); 
        public IEnumerable<string> SortCoaches(IEnumerable<string> names)
        {
            var results = _setter.GetSortOrder().Where(name => names.Any(otherName => name == otherName)).ToList();

            foreach (var name in names)
            {
                if (!results.Contains(name))
                {
                    results.Add(name);
                }
            }

            return results;
        }
    }
}