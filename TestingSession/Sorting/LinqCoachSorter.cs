using System.Collections.Generic;
using System.Linq;

namespace TestingSession.Sorting
{
    public class LinqCoachSorter : ICoachSorter
    {
        public IEnumerable<string> SortCoaches(IEnumerable<string> names)
        {
            return names.OrderBy(x => x);
        }
    }
}