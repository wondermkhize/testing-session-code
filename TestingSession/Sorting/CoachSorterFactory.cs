
namespace TestingSession.Sorting
{
    public class CoachSorterFactory
    {
        private const string SortOrder = "desc";
        private const string FixedOrder = "fixed";

        public ICoachSorter CreateSorter(string type)
        { 
            if (type == SortOrder)
            {
                return new LinqCoachSorter();
            }

            if (type == FixedOrder)
            {
                return new HardCodedCoarchSorter();
            }

            return new DefaultCoachSorter();
        }
    }
}