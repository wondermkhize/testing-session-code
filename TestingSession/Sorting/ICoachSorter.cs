using System.Collections.Generic;

namespace TestingSession.Sorting
{
    public interface ICoachSorter
    {
        IEnumerable<string> SortCoaches(IEnumerable<string> names);
    }
}