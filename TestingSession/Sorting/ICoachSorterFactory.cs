
namespace TestingSession.Sorting
{
    public interface ICoachSorterFactory
    {
        ICoachSorter CreateSorter(string type);
    }
}