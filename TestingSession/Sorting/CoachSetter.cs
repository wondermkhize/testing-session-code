using System.Collections.Generic;

namespace TestingSession.Sorting
{
    public class CoachSetter
    {
        public IEnumerable<string> GetStaff()
        {
            HashSet<string> _staff = new HashSet<string>
            {
                "Steven",
                "Ze",
                "Simon",
                "Bernardt",
                "Matthew",
                "Lindo",
                "Desmond",
                "Braiden",
                "Verne",
                "Noluthando",
                "Pebble"
            };

            return _staff;
        }

        public IEnumerable<string> GetSortOrder()
        {
            HashSet<string> _sortOrder = new HashSet<string>
            {
                "Pebble",
                "Noluthando",
                "Steven"
            };

            return _sortOrder;
        }
    }
}