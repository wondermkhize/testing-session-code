using System.Collections.Generic;
using TestingSession.Sorting;

namespace TestingSession
{
    public class CoachService
    {
        private const string FixedOrder = "fixed";
        private const string SortOrder = "desc";
        private CoachSetter _staff;

        private readonly ICoachSorterFactory _sorterFactory;
        public CoachService(ICoachSorterFactory sorterFactory)
        {
            _sorterFactory = sorterFactory;
            _staff = new CoachSetter();
        }
        
        public IEnumerable<string> GetCoaches(bool byRank = false)
        {
            var sortType = byRank ?  FixedOrder : SortOrder;
            var sorter = _sorterFactory.CreateSorter(sortType);
            return sorter.SortCoaches(_staff.GetStaff());
        }
    }
}