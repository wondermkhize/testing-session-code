using Xunit;
using TestingSession;
using TestingSession.Sorting;

namespace TestingSession.Tests
{
    public class CoachServiceTester
    {
        [Fact]
        public void GetCoaches_ByRank_CreatesAFixedSorter()
        {
            var mockFactory = new MockCoachSorterFactory();
            var coachService = new CoachService(mockFactory);
            coachService.GetCoaches(byRank: true);

            Assert.True(mockFactory.TypeFromLastCreateCall == "fixed", $"Sorter was {mockFactory.TypeFromLastCreateCall} and not the hard-coded version");
        }

        [Fact]
        public void GetCoaches_NotByRank_CreatesAFixedSorter()
        {
            var mockFactory = new MockCoachSorterFactory();
            var coachService = new CoachService(mockFactory);
            coachService.GetCoaches(byRank: false);

            Assert.True(mockFactory.TypeFromLastCreateCall == "desc", $"Sorter was {mockFactory.TypeFromLastCreateCall} and not the LINQ version as expected");
        }
    }
}