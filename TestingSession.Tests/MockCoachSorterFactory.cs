using System;
using TestingSession;
using TestingSession.Sorting;

namespace TestingSession
{
    public class MockCoachSorterFactory : ICoachSorterFactory
    {
        private string _lastCalledType;

        public string TypeFromLastCreateCall 
        {
            get 
            {
                return _lastCalledType;
            }
        }

        public ICoachSorter CreateSorter(string type)
        {
            _lastCalledType = type;
            return new DefaultCoachSorter();
        }
    }
}