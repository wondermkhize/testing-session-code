using System.Collections.Generic;
using System.Linq;
using Xunit;
using TestingSession;
using TestingSession.Sorting;

namespace TestingSession.Tests.Sorting
{
    public class LinqCoachSorterTester
    {
        private LinqCoachSorter _sorter;

        [SetUp]
        public void Setup()
        {
             _sorter = new LinqCoachSorter();
        }

        [Fact]
        public void SortCoaches_WithListOfMatthewAndFred_ReturnsNewObject()
        {
            var message = "Results not the same as input"
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results != items , message);
        }

        [Fact]
        public void SortCoaches_WithListOfMatthewAndFred_ResultsHaveTheSameNumberOfItems()
        {
            var message = "Results have the incorrect count";
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.Count() == 2 , message);
        }

        [Fact]
        public void SortCoaches_WithListOfMatthewFredAndCrystal_ResultsAreInDescendingOrder()
        {
            var items = new List<string> { "Matthew", "Fred", "Crystal", "Abigail" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.ElementAt(0) == "Abigail", $"{results.ElementAt(0)} at start of list instead of Abigail");
            Assert.True(results.ElementAt(1) == "Crystal", $"{results.ElementAt(1)} at start of list instead of Crystal");
            Assert.True(results.ElementAt(2) == "Fred", $"{results.ElementAt(2)} at start of list instead of Fred");
            Assert.True(results.ElementAt(3) == "Matthew", $"{results.ElementAt(3)} at start of list instead of Matthew");
        }
    }
}