using System.Collections.Generic;
using System.Linq;
using Xunit;
using TestingSession;
using TestingSession.Sorting;

namespace TestingSession.Tests.Sorting
{
    public class HardCodedSorterTester
    {
        private ICoachSorter _sorter;

        [SetUp]
        public void Setup()
        {
            _sorter = new HardCodedCoarchSorter();
        }

        [Fact]
        public void SortCoaches_WithNamesNotInHardCodedList_ReturnsDistinctObject()
        {
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results != items , "Results not the same as input");
        }

        [Fact]
        public void SortCoaches_WithNamesNotInHardCodedList_ResultsHaveTheSameNumberOfItems()
        {
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.Count() == 2 , "Results have the incorrect count");
        }

        [Fact]
        public void SortCoaches_WithNamesNotInHardCodedList_DoesNotSortItems()
        {
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.ElementAt(0) == "Matthew", $"{results.ElementAt(0)} at start of list instead of Matthew");
            Assert.True(results.ElementAt(1) == "Fred", $"{results.ElementAt(1)} at start of list instead of Fred");
        }

        [Fact]
        public void SortCoaches_WithNamesInHardCodedList_SortsItemsAccordingToInternalSortOrder()
        {
            var items = new List<string> { "Noluthando", "Pebble", "Steven" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.ElementAt(0) == "Pebble", "Pebble not at start of list");
            Assert.True(results.ElementAt(1) == "Noluthando", "Noluthando not in middle of list");
            Assert.True(results.ElementAt(2) == "Steven", "Steven not at end of list");
        }

        [Fact]
        public void SortCoaches_WithNamesInHardCodedListAndExtraNamesAtStart_SortsExtraItemsAfterInternalNames()
        {
            var items = new List<string> { "Jacob", "Noluthando", "Pebble", "Steven" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.ElementAt(0) == "Pebble", "Pebble not at start of list");
            Assert.True(results.ElementAt(1) == "Noluthando", "Noluthando not in middle of list");
            Assert.True(results.ElementAt(2) == "Steven", "Steven not third in the list");
            Assert.True(results.ElementAt(3) == "Jacob", "Jacob not at end of list");
        }

        [Fact]
        public void SortCoaches_WithNamesInHardCodedListAndExtraNamesAtEnd_SortsExtraItemsAfterInternalNames()
        {
            var items = new List<string> { "Noluthando", "Pebble", "Steven", "Jacob" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.ElementAt(0) == "Pebble", "Pebble not at start of list");
            Assert.True(results.ElementAt(1) == "Noluthando", "Noluthando not in middle of list");
            Assert.True(results.ElementAt(2) == "Steven", "Steven not third in the list");
            Assert.True(results.ElementAt(3) == "Jacob", $"{results.ElementAt(3)} at end of list instead of Jacob");
        }
    }
}