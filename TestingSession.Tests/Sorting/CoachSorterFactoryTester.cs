using Xunit;
using TestingSession;
using TestingSession.Sorting;

namespace TestingSession.Tests.Sorting
{
    public class CoachSorterFactoryTester
    {
        private ICoachSorterFactory _sorterFactory;

        [SetUp]
        public void Setup()
        {
            _sorterFactory = new CoachSorterFactory();
        }

        [Fact]
        public void CreateSorter_WithTypeSetToDesc_ReturnsALinqCoachSorter()
        {
            var sortingOrderMsg = "Sorter should be LINQ sorter";
            var orderType = "desc";
            var result = _sorterFactory.CreateSorter(orderType);

            Assert.True(result.GetType() == typeof(LinqCoachSorter), sortingOrderMsg);
        }

        [Fact]
        public void CreateSorter_WithTypeSetToFixed_ReturnsAHardCodedCoachSorter()
        {   
            var sortingOrderMsg = "Sorter should be hard-coded sorter";
            var orderType = "fixed";
            var result = _sorterFactory.CreateSorter(orderType);

            Assert.True(result.GetType() == typeof(HardCodedCoarchSorter), sortingOrderMsg);
        }

        [Fact]
        public void CreateSorter_WithEmptyType_ReturnsDefaultCoachSorter()
        {
            var sortingOrderMsg = "Sorter should be the default sorter";
            var result = _sorterFactory.CreateSorter(string.Empty);

            Assert.True(result.GetType() == typeof(DefaultCoachSorter), sortingOrderMsg);
        }

        [Fact]
        public void CreateSorter_WithTypeSetToRandom_ReturnsDefaultCoachSorter()
        {
            var orderType = "random";
            var sortingOrderMsg = "Sorter should be the default sorter";
            var result = _sorterFactory.CreateSorter(orderType);

            Assert.True(result.GetType() == typeof(DefaultCoachSorter), sortingOrderMsg);
        }
    }
}