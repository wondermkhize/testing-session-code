using System.Collections.Generic;
using System.Linq;
using Xunit;
using TestingSession;
using TestingSession.Sorting;

namespace TestingSession.Tests.Sorting
{
    public class DefaultCoachSorterTester
    {
        private ICoachSorter _sorter;

        [SetUp]
        public void Setup()
        {
            _sorter = new DefaultCoachSorter();
        }

        [Fact]
        public void SortCoaches_WithListOfMatthewAndFred_ReturnsCollectionAsIs()
        {
            const string exceptionMsg = "Sorted values were modified unexpectedly";
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results == items, exceptionMsg);
        }

        [Fact]
        public void SortCoaches_WithListOfMatthewAndFred_DoesNotChangeTheCollectionOrder()
        {
            const string exceptionMsg = "Sorted values were modified unexpectedly";
            var items = new List<string> { "Matthew", "Fred" };
            var results = _sorter.SortCoaches(items);

            Assert.True(results.ElementAt(0) == items[0], exceptionMsg);
            Assert.True(results.ElementAt(1) == items[1], exceptionMsg);
        }
    }
}