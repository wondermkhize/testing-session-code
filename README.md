Added .gitignore file.
Added README file.
Took out magic strings from tests and classes.
Created CoachSetter class with two methods to return hash setter list.
Included const variables at class level.
Added SetUp methods in tests classes.